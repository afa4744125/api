<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250211121906 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE data (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', player_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', created_by CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', value VARCHAR(180) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', dataType_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_ADF3F36399E6F5DF (player_id), INDEX IDX_ADF3F363933C6395 (dataType_id), INDEX IDX_ADF3F363DE12AB56 (created_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F36399E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F363933C6395 FOREIGN KEY (dataType_id) REFERENCES data_type (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F363DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F36399E6F5DF');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F363933C6395');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F363DE12AB56');
        $this->addSql('DROP TABLE data');
    }
}
