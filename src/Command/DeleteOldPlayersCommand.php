<?php

namespace App\Command;

use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteOldPlayersCommand extends Command
{
    protected static $defaultName = 'delete:old-players';
    private $playerRepository;
    private $entityManager;

    public function __construct(PlayerRepository $playerRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->playerRepository = $playerRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->setName('delete:old-players')
            ->setDescription('Delete players marked as deleted for more than 7 days.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $sevenDaysAgo = new \DateTime('-7 days');
        $playersToDelete = $this->playerRepository->findPlayersToDelete($sevenDaysAgo);

        foreach ($playersToDelete as $player) {
            $this->entityManager->remove($player);
        }

        $this->entityManager->flush();

        $output->writeln(count($playersToDelete) . ' Player delete.');
        return Command::SUCCESS;
    }
}
