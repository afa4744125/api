<?php

namespace App\Command;

use App\Repository\TeamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteOldTeamsCommand extends Command
{
    protected static $defaultName = 'delete:old-teams';
    private $teamRepository;
    private $entityManager;

    public function __construct(TeamRepository $teamRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->teamRepository = $teamRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->setName('delete:old-teams')
            ->setDescription('Delete teams marked as deleted for more than 7 days.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $sevenDaysAgo = new \DateTime('-7 days');
        $teamsToDelete = $this->teamRepository->findTeamsToDelete($sevenDaysAgo);

        foreach ($teamsToDelete as $team) {
            $this->entityManager->remove($team);
        }

        $this->entityManager->flush();

        $output->writeln(count($teamsToDelete) . ' teams deleted.');
        return Command::SUCCESS;
    }
}
