<?php

namespace App\Entity;

use Ramsey\Uuid\Uuid;
use App\Repository\PlayerRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Organisation;
use App\Entity\Team;
use DateTimeImmutable;

enum Gender: string
{
    case MAN = 'man';
    case WOMAN = 'woman';
}

#[ORM\Entity(repositoryClass: PlayerRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Player
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    #[ORM\Column(type: 'uuid', unique: true)]
    private ?string $id = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $relationshipnumber = null;

    #[ORM\Column(length: 180)]
    private ?string $membershipnumber = null;

    #[ORM\Column(length: 180)]
    private ?string $firstname = null;

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $infix = null;

    #[ORM\Column(length: 180)]
    private ?string $lastname = null;

    #[ORM\Column(enumType: Gender::class, nullable: true)]
    private ?Gender $gender = null;

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(type: 'date')]
    private ?\DateTimeInterface $dateOfBirth = null;

    #[ORM\ManyToOne(targetEntity: Organisation::class)]
    #[ORM\JoinColumn(name: 'org_id', referencedColumnName: 'id', nullable: true)] 
    private ?Organisation $org_id = null;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(name: 'team_id', referencedColumnName: 'id', nullable: true)] 
    private ?Team $team_id = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private ?DateTimeImmutable $updatedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $deletedAt = null;

    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getRelationshipnumber():?string{
        return $this->relationshipnumber;
    }

    public function setRelationshipnumber(?string $relationshipnumber): self
    {
        $this->relationshipnumber = $relationshipnumber;
        return $this;
    }

    public function getMembershipnumber():?string
    {
        return $this->membershipnumber;
    }

    public function setMembershipnumber(?string $membershipnumber): self
    {
        $this->membershipnumber = $membershipnumber;
        return $this;
    }

    public function getFirstname():?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getInfix():?string
    {
        return $this->infix;
    }

    public function setInfix(?string $infix): self
    {
        $this->infix = $infix;
        return $this;
    }

    public function getLastname():?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;
        return $this;
    }

    public function getGender():?Gender
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $genderEnum = Gender::tryFrom($gender);
        
        if ($genderEnum !== null) {
            $this->gender = $genderEnum;
        } else {
            throw new \InvalidArgumentException("Invalid gender");
        }
    
        return $this;
    }
    
    public function getEmail():?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth->format('d-m-Y');
    }

    public function setDateOfBirth(string $dateOfBirth): self
    {
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $dateOfBirth)) {
            $dateOfBirth = \DateTime::createFromFormat('Y-m-d', $dateOfBirth)->format('d-m-Y');
        }
    
        $this->dateOfBirth = \DateTime::createFromFormat('d-m-Y', $dateOfBirth);
    
        return $this;
    }
    

    public function getTeam_id():?string
    {
        return $this->team_id? $this->team_id->getId() : null;
    }

    public function setTeam_id(?Team $team_id): self
    {
        $this->team_id = $team_id;
        return $this;
    }

    public function getTeam_name(): ?string
    {
        return $this->team_id ? $this->team_id->getName() : null;
    }
    
    public function getOrg_id(): ?string 
    {
        return $this->org_id ? $this->org_id->getId() : null;
    }


    public function setOrg_id(?Organisation $org_id): self
    {
        $this->org_id = $org_id;
        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getDeletedAt(): ?DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTimeImmutable $deletedAt): static
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new DateTimeImmutable();
    }
}
