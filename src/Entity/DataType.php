<?php

namespace App\Entity;

use Ramsey\Uuid\Uuid;
use App\Repository\DataTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use DateTimeImmutable;
use InvalidArgumentException;

enum statuses: string
{
    case active = 'active';
    case inactive = 'inactive';
}

#[ORM\Entity(repositoryClass: DataTypeRepository::class)]
#[ORM\HasLifecycleCallbacks]
class DataType
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    #[ORM\Column(type: 'uuid', unique: true)]
    private ?string $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $name = null;

    #[ORM\Column(enumType: statuses::class, nullable: true)]
    private ?statuses $status = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $number = null;


    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private ?DateTimeImmutable $updatedAt = null;

    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->status = statuses::inactive; 
        $this->number = true;

    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getStatus(): ?statuses
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {

        $statusEnum = statuses::tryFrom($status);
        
        if ($statusEnum !== null) {
            $this->status = $statusEnum;
        } else {
            throw new \InvalidArgumentException("Invalid status");
        }
    
        return $this;
    }

    public function getNumber(): ?bool
    {
        return $this->number;
    }

    public function setNumber(bool $number): static
    {
        $this->number = $number;
        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new DateTimeImmutable();
    }
}
