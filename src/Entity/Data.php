<?php

namespace App\Entity;

use App\Entity\User;
use App\Entity\DataType;
use App\Entity\Player;
use Ramsey\Uuid\Uuid;
use App\Repository\DataRepository;
use Doctrine\ORM\Mapping as ORM;
use DateTimeImmutable;

#[ORM\Entity(repositoryClass: DataRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Data
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    #[ORM\Column(type: 'uuid', unique: true)]
    private ?string $id = null;

    #[ORM\Column(length: 180)]
    private ?string $value = null;

    #[ORM\ManyToOne(targetEntity: Player::class)]
    #[ORM\JoinColumn(name: "player_id", referencedColumnName: "id", nullable: false)]
    private ?Player $player_id = null;

    #[ORM\ManyToOne(targetEntity: DataType::class)]
    #[ORM\JoinColumn(name: "dataType_id", referencedColumnName: "id", nullable: false)]
    private ?DataType $dataType_id = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: "created_by", referencedColumnName: "id", nullable: false)]
    private ?User $createdBy = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private ?DateTimeImmutable $updatedAt = null;

    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;

        return $this;
    }

    public function getPlayer_id(): ?string
    {
        return $this->player_id? $this->player_id->getId() : null;
    }

    public function setPlayer_id(Player $player_id): static
    {
        $this->player_id = $player_id;

        return $this;
    }

    public function getDataType_id(): ?DataType
    {
        return $this->dataType_id;
    }

    public function setDataType_id(DataType $dataType_id): static
    {
        $this->dataType_id = $dataType_id;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $createdBy): static
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new DateTimeImmutable();
    }
}
