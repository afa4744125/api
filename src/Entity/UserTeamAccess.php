<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use App\Entity\User;
use App\Entity\Team;
use DateTimeImmutable;

#[ORM\Entity]
#[ORM\Table(name: "user_team_access")]
class UserTeamAccess
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    #[ORM\Column(type: 'uuid', unique: true)]
    private ?string $id = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: "user_id", referencedColumnName: "id", nullable: false)]
    private ?User $user_id = null;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(name: "team_id", referencedColumnName: "id", nullable: false)]
    private ?Team $team_id = null;

    #[ORM\Column(type: "datetime_immutable", nullable: false)]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: "datetime_immutable", nullable: false)]
    private ?DateTimeImmutable $updatedAt = null;

    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUser_id(): ?User
    {
        return $this->user_id ? $this->user_id->getId() : null;
    }

    public function setUser_id(User $user_id): self
    {
        $this->user_id = $user_id;
        return $this;
    }

    public function getTeam_id(): ?string
    {
        return $this->team_id ? $this->team_id->getId() : null;
    }

    public function setTeam_id(Team $team_id): self
    {
        $this->team_id = $team_id;
        return $this;
    }

    public function getTeam(): ?Team{
        return $this->team_id;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new DateTimeImmutable();
    }
}
