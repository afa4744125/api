<?php
namespace App\Service;

use App\Entity\Player;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PlayerRepository;

class PlayerService
{
    private PlayerRepository $playerRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, PlayerRepository $playerRepository)
    {
        $this->entityManager = $entityManager;
        $this->playerRepository = $playerRepository;
    }

    public function getPlayers(string $org_id, int $pagesize, int $offset, ?string $membershipnumber = null, ?string $relationshipnumber = null ,?string $firstname = null, ?string $lastname = null,?string $email = null, ?array $teams = null, ?string $gender = null, ?string $dateofbirth = null): array
    {
        return $this->playerRepository->getPlayers($org_id, $pagesize, $offset, $membershipnumber, $relationshipnumber, $firstname, $lastname, $email, $teams, $gender, $dateofbirth); 
    }

    public function getPlayersCount(string $org_id , ?string $membershipnumber = null, ?string $relationshipnumber = null , ?string $firstname = null, ?string $lastname = null, ?string $email = null, ?array $teams = null, ?string $gender = null, ?string $dateofbirth = null): int
    {
        return $this->playerRepository->getPlayersCount($org_id, $membershipnumber, $relationshipnumber, $firstname, $lastname, $email, $teams, $gender, $dateofbirth); 
    }

    public function getPlayersCountDelete(string $org_id): int
    {
        return $this->playerRepository->getPlayersCountDelete($org_id); 
    }


    public function createPLayer(Player $player): void
    {
        $this->entityManager->persist($player);
        $this->entityManager->flush();
    }

    public function deletePlayer(Player $player): void
    {
        $player->setDeletedAt(new \DateTimeImmutable());
        $this->entityManager->persist($player);
        $this->entityManager->flush();
    }

    public function getplayerById(string $id): ?Player
    {
        return $this->playerRepository->findById($id); 
    }

    public function updatePlayer(Player $player): void
    {
        $this->entityManager->flush();
    }

}
