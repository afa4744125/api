<?php
namespace App\Service;

use App\Entity\Organisation;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\OrganisationRepository;

class OrganisationService
{
    private OrganisationRepository $organisationRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, OrganisationRepository $organisationRepository)
    {
        $this->entityManager = $entityManager;
        $this->organisationRepository = $organisationRepository;
    }

    public function getAllOrganisations(): array
    {
        return $this->organisationRepository->findAllActive();
    }

    public function getOrganisationById(string $id): ?Organisation
    {
        return $this->organisationRepository->findActiveById($id); 
    }

    public function createOrganisation(Organisation $organisation): void
    {
        $this->entityManager->persist($organisation);
        $this->entityManager->flush();
    }

    public function updateOrganisation(Organisation $organisation): void
    {
        $this->entityManager->flush();
    }

    public function deleteOrganisation(Organisation $organisation): void
    {
        $organisation->setDeletedAt(new \DateTimeImmutable());
        $this->entityManager->persist($organisation);
        $this->entityManager->flush();
    }
}
