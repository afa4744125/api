<?php
namespace App\Service;

use App\Entity\Team;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\TeamRepository;

class TeamService
{
    private TeamRepository $teamRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, TeamRepository $teamRepository)
    {
        $this->entityManager = $entityManager;
        $this->teamRepository = $teamRepository;
    }

    public function getTeamsById(string $id): array
    {
        return $this->teamRepository->findAllById($id); 
    }

    public function createTeam(Team $team): void
    {
        $this->entityManager->persist($team);
        $this->entityManager->flush();
    }

    public function deleteTeam(Team $team): void
    {
        $team->setDeletedAt(new \DateTimeImmutable());
        $this->entityManager->persist($team);
        $this->entityManager->flush();
    }

    public function getTeamById(string $id): ?Team
    {
        return $this->teamRepository->findById($id); 
    }

    public function updateTeam(Team $team): void
    {
        $this->entityManager->flush();
    }

    public function getTeamsCount(string $org_id): int
    {
        return $this->teamRepository->getTeamsCount($org_id); 
    }

}
