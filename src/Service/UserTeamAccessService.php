<?php
namespace App\Service;

use App\Entity\UserTeamAccess;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserTeamAccessRepository;

class UserTeamAccessService
{
    private UserTeamAccessRepository $userTeamAccessRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, UserTeamAccessRepository $userTeamAccessRepository)
    {
        $this->entityManager = $entityManager;
        $this->userTeamAccessRepository = $userTeamAccessRepository;
    }

    public function findAllByUserIdGetIds(string $userId): array
    {
        $userTeamAccesses = $this->userTeamAccessRepository->findAllByUserId($userId);

        return array_map(
            fn($userTeamAccess) => $userTeamAccess->getTeam_id(),
            $userTeamAccesses
        );
        
    }    

    public function findAllByUserIdGetTeams(string $userId): array
    {
        $userTeamAccesses = $this->userTeamAccessRepository->findAllByUserId($userId);

        return array_map(
            fn($userTeamAccess) => $userTeamAccess->getTeam(),
            $userTeamAccesses
        );
    }    

    public function hasUserAccessToTeam(string $user_id, string $team_id): bool
    {   
        $access = $this->userTeamAccessRepository->findOneBy([
            'user_id' => $user_id,
            'team_id' => $team_id,
        ]);

        return $access !== null;
    }


    public function createUserTeamAccess(UserTeamAccess $userTeamAccess): void
    {
        $this->entityManager->persist($userTeamAccess);
        $this->entityManager->flush();
    }

    public function updateUserTeamAccess(UserTeamAccess $userTeamAccess): void
    {
        $this->entityManager->flush();
    }

    public function deleteAllByUserId( string $userId){
        $this->userTeamAccessRepository->deleteAllByUserId($userId);
        $this->entityManager->flush();
    }
}
