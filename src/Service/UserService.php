<?php
namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserRepository;

class UserService
{
    private UserRepository $userRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function getAllUsers(string $org_id): array
    {
        return $this->userRepository->findAllActive($org_id);
    }

    public function getUserById(string $id): ?User
    {
        return $this->userRepository->findActiveById($id); 
    }

    public function createUser(User $User): void
    {
        $this->entityManager->persist($User);
        $this->entityManager->flush();
    }

    public function updateUser(User $User): void
    {
        $this->entityManager->flush();
    }

    public function deleteUser(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
    
    public function getUsersCount(string $org_id): int{
        return $this->userRepository->getUsersCount($org_id);
    }
}
