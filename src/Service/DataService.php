<?php
namespace App\Service;

use App\Entity\Data;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\DataRepository;

class DataService
{
    private DataRepository $dataRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, DataRepository $dataRepository)
    {
        $this->entityManager = $entityManager;
        $this->dataRepository = $dataRepository;
    }
        
    public function getPublicListDataType(): array
    {
        return $this->dataRepository->getPublicListDataType();
    }

    public function getDataById(string $id): ?Data
    {
        return $this->dataRepository->findById($id); 
    }

    public function getDataOfPlayer(string $id): array
    {
        return $this->dataRepository->findByPlayerId($id); 
    }

    public function createData(Data $Data): void
    {
        $this->entityManager->persist($Data);
        $this->entityManager->flush();
    }

    public function updateData(): void
    {
        $this->entityManager->flush();
    }

    public function deleteData(Data $Data): void
    {
        $this->entityManager->remove($Data);
        $this->entityManager->flush();
    }
}
