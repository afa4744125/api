<?php
namespace App\Service;

use App\Entity\DataType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\DataTypeRepository;

class DataTypeService
{
    private DataTypeRepository $dataTypeRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, DataTypeRepository $dataTypeRepository)
    {
        $this->entityManager = $entityManager;
        $this->dataTypeRepository = $dataTypeRepository;
    }
        
    public function getPublicListDataType(): array
    {
        return $this->dataTypeRepository->getPublicListDataType();
    }

    public function getAdminListDataType(): array
    {
        return $this->dataTypeRepository->GetAllDataType();
    }

    public function getDataTypeById(string $id): ?DataType
    {
        return $this->dataTypeRepository->findById($id); 
    }

    public function createDataType(DataType $DataType): void
    {
        $this->entityManager->persist($DataType);
        $this->entityManager->flush();
    }

    public function updateDataType(): void
    {
        $this->entityManager->flush();
    }

    public function deleteDataType(DataType $DataType): void
    {
        $this->entityManager->remove($DataType);
        $this->entityManager->flush();
    }
}
