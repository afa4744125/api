<?php

namespace App\Repository;

use App\Entity\Team;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Team>
 */
class TeamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Team::class);
    }

    public function findAllById(string $orgId)
    {
        $results = $this->createQueryBuilder('o')
            ->andWhere('o.org_id = :id')
            ->andWhere('o.deletedAt IS NULL')
            ->setParameter('id', $orgId)
            ->getQuery()
            ->getResult();
    
        usort($results, function ($a, $b) {
            $nameA = $a->getName();
            $nameB = $b->getName();

            preg_match('/^(.*)_([0-9]+)$/', $nameA, $partsA);
            preg_match('/^(.*)_([0-9]+)$/', $nameB, $partsB);
    
            if (!isset($partsA[1], $partsA[2]) || !isset($partsB[1], $partsB[2])) {
                return strcmp($nameA, $nameB);
            }
    
            $cmpPrefix = strcmp($partsA[1], $partsB[1]);
            if ($cmpPrefix === 0) {
                return intval($partsA[2]) - intval($partsB[2]);
            }
            
            return $cmpPrefix;
        });
    
        return $results;
    }

    public function findById(string $id): ?Team
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.id = :id')
            ->andWhere('o.deletedAt IS NULL')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getTeamByIdAndOrgId(string $id, string $org_id): ?Team
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.org_id = :org_id')
            ->andWhere('o.id = :id')
            ->andWhere('o.deletedAt IS NULL')
            ->setParameter('id', $id)
            ->setParameter('org_id', $org_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getTeamByNameAndOrgId(string $name, string $org_id): ?Team
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.org_id = :org_id')
            ->andWhere('o.name = :name')
            ->andWhere('o.deletedAt IS NULL')
            ->setParameter('name', $name)
            ->setParameter('org_id', $org_id)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function getTeamsCount(string $org_id): int
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->andWhere('o.deletedAt IS NULL')
            ->andWhere('o.org_id = :org_id')
            ->setParameter('org_id', $org_id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findTeamsToDelete(\DateTime $sevenDaysAgo): array
    {
    return $this->createQueryBuilder('t')
        ->where('t.deletedAt IS NOT NULL')
        ->andWhere('t.deletedAt <= :sevenDaysAgo')
        ->setParameter('sevenDaysAgo', $sevenDaysAgo)
        ->getQuery()
        ->getResult();
    }

}
