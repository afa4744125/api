<?php

namespace App\Repository;

use App\Entity\UserTeamAccess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserTeamAccess>
 */
class UserTeamAccessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserTeamAccess::class);
    }

    public function findAllByUserId(string $userId): array
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.user_id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
    }
    
    public function findById(string $id): ?UserTeamAccess
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function deleteAllByUserId(string $userId): int
    {
        return $this->createQueryBuilder('o')
            ->delete()
            ->andWhere('o.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->getQuery()
            ->execute();
    }

    public function existsByUserId(string $userId): bool
    {
        $result = $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->andWhere('o.user_id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getSingleScalarResult();
    
        return (int)$result > 0;
    }    

}
