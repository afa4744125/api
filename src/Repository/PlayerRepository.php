<?php

namespace App\Repository;

use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Team>
 */
class PlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Player::class);
    }

    public function getPlayers(
        string $org_id,
        int $pagesize,
        int $offset,
        ?string $membershipnumber = null,
        ?string $relationshipnumber = null,
        ?string $firstname = null,
        ?string $lastname = null,
        ?string $email = null,
        ?array $teams = null,
        ?string $gender = null,
        ?string $dateofbirth = null
    ) {
        $queryBuilder = $this->createQueryBuilder('o')
            ->andWhere('o.deletedAt IS NULL')
            ->andWhere('o.org_id = :org_id')
            ->setParameter('org_id', $org_id)
            ->orderBy('o.createdAt', 'DESC');
    
        if ($membershipnumber) {
            $queryBuilder->andWhere('o.membershipnumber LIKE :membershipnumber')
                ->setParameter('membershipnumber', '%' . $membershipnumber . '%');
        }
    
        if ($relationshipnumber) {
            $queryBuilder->andWhere('o.relationshipnumber LIKE :relationshipnumber')
                ->setParameter('relationshipnumber', '%' . $relationshipnumber . '%');
        }
    
        if ($firstname) {
            $queryBuilder->andWhere('o.firstname LIKE :firstname')
                ->setParameter('firstname', '%' . $firstname . '%');
        }
    
        if ($lastname) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->like('o.lastname', ':lastname'),
                    $queryBuilder->expr()->like(
                        $queryBuilder->expr()->concat('o.infix', $queryBuilder->expr()->literal(' '), 'o.lastname'),
                        ':lastname'
                    )
                )
            )
            ->setParameter('lastname', '%' . $lastname . '%');
        }
    
        if ($email) {
            $queryBuilder->andWhere('o.email LIKE :email')
                ->setParameter('email', '%' . $email . '%');
        }
    
        if (!empty($teams) && $teams !== [""]) {
            $queryBuilder->andWhere('o.team_id IN (:teams)')
                ->setParameter('teams', $teams);
        }
    
        if ($gender) {
            $queryBuilder->andWhere('o.gender = :gender')
                ->setParameter('gender', $gender);
        }
    
        if ($dateofbirth) {
            $startOfYear = (new \DateTime($dateofbirth . '-01-01'))->format('Y-m-d');
            $endOfYear = (new \DateTime($dateofbirth . '-12-31'))->format('Y-m-d');
            
            $queryBuilder->andWhere('o.dateOfBirth BETWEEN :startOfYear AND :endOfYear')
                ->setParameter('startOfYear', $startOfYear)
                ->setParameter('endOfYear', $endOfYear);
        }
    
        $queryBuilder->setFirstResult($offset)
                     ->setMaxResults($pagesize);
    
        $results = $queryBuilder
            ->getQuery()
            ->getResult();
    
        return $results;
    }
    
    

    public function getPlayersCount(
        string $org_id,
        ?string $membershipnumber = null,
        ?string $relationshipnumber = null,
        ?string $firstname = null,
        ?string $lastname = null,
        ?string $email = null,
        ?array $teams = null,
        ?string $gender = null,
        ?string $dateofbirth = null
    ) {
        $queryBuilder = $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->andWhere('o.deletedAt IS NULL')
            ->andWhere('o.org_id = :org_id')
            ->setParameter('org_id', $org_id);
    
            if ($membershipnumber) {
                $queryBuilder->andWhere('o.membershipnumber LIKE :membershipnumber')
                    ->setParameter('membershipnumber', '%' . $membershipnumber. '%');
            }

            if ($relationshipnumber) {
                $queryBuilder->andWhere('o.relationshipnumber LIKE :relationshipnumber')
                    ->setParameter('relationshipnumber', '%' . $relationshipnumber. '%');
            }

            if ($firstname) {
                $queryBuilder->andWhere('o.firstname LIKE :firstname')
                    ->setParameter('firstname', '%' . $firstname . '%');
            }

            if ($lastname) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->like('o.lastname', ':lastname'),
                        $queryBuilder->expr()->like(
                            $queryBuilder->expr()->concat('o.infix', $queryBuilder->expr()->literal(' '), 'o.lastname'),
                            ':lastname'
                        )
                    )
                )
                ->setParameter('lastname', '%' . $lastname . '%');
            }

            if ($email) {
                $queryBuilder->andWhere('o.email LIKE :email')
                    ->setParameter('email', '%' . $email. '%');
            }

            if (!empty($teams) && $teams !== [""]) {
                $queryBuilder->andWhere('o.team_id IN (:teams)')
                    ->setParameter('teams', $teams);
            }

            if ($gender) {
                $queryBuilder->andWhere('o.gender = :gender')
                    ->setParameter('gender', $gender);
            }

            if ($dateofbirth) {
                $startOfYear = (new \DateTime($dateofbirth . '-01-01'))->format('Y-m-d');
                $endOfYear = (new \DateTime($dateofbirth . '-12-31'))->format('Y-m-d');
            
                $queryBuilder->andWhere('o.dateOfBirth BETWEEN :startOfYear AND :endOfYear')
                    ->setParameter('startOfYear', $startOfYear)
                    ->setParameter('endOfYear', $endOfYear);
            }

        return (int) $queryBuilder
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function getPlayersCountDelete(string $org_id): int
    {
        return $this->createQueryBuilder('o')
        ->select('COUNT(o.id)')
        ->andWhere('o.deletedAt IS NOT NULL')
        ->andWhere('o.org_id = :org_id')
        ->setParameter('org_id', $org_id)
        ->getQuery()
        ->getSingleScalarResult();
    }


    public function findById(string $id): ?Player
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.id = :id')
            ->andWhere('o.deletedAt IS NULL')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findPlayersToDelete(\DateTime $sevenDaysAgo): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.deletedAt IS NOT NULL')
            ->andWhere('p.deletedAt <= :sevenDaysAgo')
            ->setParameter('sevenDaysAgo', $sevenDaysAgo)
            ->getQuery()
            ->getResult();
    }

}
