<?php

namespace App\Controller\Player;

use App\Entity\Player;
use App\Entity\Team;
use App\Repository\OrganisationRepository;
use App\Repository\TeamRepository;
use App\Service\PlayerService;
use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/player/import')]
class ImportPlayerController extends AbstractController
{
    private TeamService $teamService;
    private PlayerService $playerService;
    private OrganisationRepository $organisationRepository;
    private TeamRepository $teamRepository;

    public function __construct(PlayerService $playerService, OrganisationRepository $organisationRepository, TeamRepository $teamRepository, TeamService $teamService)
    {
        $this->playerService = $playerService;
        $this->organisationRepository = $organisationRepository;
        $this->teamRepository = $teamRepository;
        $this->teamService = $teamService;
    }

    #[Route('/', name: 'api_player_multi_create', methods: ['POST'])]
    public function multiCreate(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $currentUser = $this->getUser();

        if (!isset($data['player']) || !is_array($data['player'])) {
            return $this->json(['error' => 'Invalid input players array is required'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $errors = [];
        $createdPlayers = [];
        $org_id = $data['org_id'];

        foreach ($data['player'] as $index => $playerData) {
            try {

                // Validate required fields
                if (empty($playerData['membershipnumber']) || empty($playerData['firstname']) || empty($playerData['lastname']) || empty($playerData['dateofbirth']) || empty($playerData['gender'])) {
                    throw new \Exception("Invalid input at index $index: Missing required fields");
                }

                $organisation = $this->organisationRepository->findActiveById($org_id);
                if (!$organisation) {
                    throw new \Exception("Invalid input at index $index: Organisation not found");
                }

                if ($currentUser->getOrg_id() !== $org_id  && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
                    throw new \Exception("Invalid input at index $index: Permission denied for organisation");
                }

                // Validate date format
                $date1 = \DateTime::createFromFormat('d-m-Y', $playerData['dateofbirth']);
                $date2 = \DateTime::createFromFormat('Y-m-d', $playerData['dateofbirth']);
                if (!$date1 && !$date2) {
                    throw new \Exception("Invalid input at index $index: Invalid date format");
                }

                $player = new Player();
                $player->setMembershipnumber($playerData['membershipnumber']);

                if (isset($playerData['relationshipnumber'])) {
                    $player->setRelationshipnumber($playerData['relationshipnumber']);
                }

                $player->setFirstname($playerData['firstname']);
                if (isset($playerData['infix'])) {
                    $player->setInfix($playerData['infix']);
                }

                $player->setLastname($playerData['lastname']);
                $player->setGender($playerData['gender']);

                if (isset($playerData['email'])) {
                    if (!filter_var($playerData['email'], FILTER_VALIDATE_EMAIL)) {
                        throw new \Exception("Invalid input at index $index: Invalid email format");
                    }
                    $player->setEmail($playerData['email']);
                }

                $player->setDateOfBirth($playerData['dateofbirth']);
                $player->setOrg_id($organisation);

                if (isset($playerData['team'])) {
                    $team = $this->teamRepository->getTeamByIdAndOrgId($playerData['team'], $org_id);
                    if (!$team) {
                        $team = $this->teamRepository->getTeamByNameAndOrgId($playerData['team'], $org_id);
                        if(!$team) {
                            $team = new Team();
                            $team->setName($playerData['team']);
                            $team->setOrg_id($organisation);
                            $this->teamService->createTeam($team);
                        }
                    }
                    $player->setTeam_id($team);
                }

                $this->playerService->createPlayer($player);
                $createdPlayers[] = ['id' => $player->getId()];
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        $response = [
            'created' => $createdPlayers,
            'errors' => $errors,
        ];

        return $this->json($response, JsonResponse::HTTP_CREATED);
    }
}
