<?php

namespace App\Controller\Player;

use App\Service\PlayerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PlayerRepository;

#[Route('/api/player')]
class DeletePlayerController extends AbstractController
{
    private PlayerService $playerService;

    public function __construct(PlayerService $playerService)
    {
        $this->playerService = $playerService;
    }

    #[Route('/{id}', name: 'api_player_delete', methods: ['DELETE'])]
    public function delete(string $id): JsonResponse
    {
        $player = $this->playerService->getPlayerById($id);
        $currentUser = $this->getUser();
    
        if (!$player) {
            return $this->json(['error' => 'player not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $player->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }
    
        $this->playerService->deletePlayer($player);
    
        return $this->json(['message' => 'player deleted']);
    }    
}
