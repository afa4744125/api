<?php

namespace App\Controller\Player;

use App\Service\PlayerService;
use App\Service\UserTeamAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/player')]
class FindPlayerController extends AbstractController
{
    private PlayerService $playerService;
    private UserTeamAccessService $userTeamAccessService;

    public function __construct(PlayerService $playerService, UserTeamAccessService $userTeamAccessService)
    {
        $this->playerService = $playerService;
        $this->userTeamAccessService = $userTeamAccessService;
    }

    #[Route('/{id}', name: 'api_player_find', methods: ['GET'])]
    public function show(string $id): JsonResponse
    {
        $player = $this->playerService->getPlayerById($id);
        $currentUser = $this->getUser();
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles());
    
        if (!$player) {
            return $this->json(['error' => 'player not found'], JsonResponse::HTTP_NOT_FOUND);
        }
    
        if ($currentUser->getOrg_id() !== $player->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisationn"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if (!$isAdmin) {
            $hasAccess = false;
        
            if ($player->getTeam_id()) {
                $hasAccess = $this->userTeamAccessService->hasUserAccessToTeam($currentUser->getId(), $player->getTeam_id());
            }
        
            if (!$hasAccess) {
                return $this->json([
                    'error' => "You do not have permission to use this team"
                ], JsonResponse::HTTP_FORBIDDEN);
            }
        }

        $data = [
            'id' => $player->getId(),
            'membershipnumber' => $player->getMembershipnumber(),
            'relationshipnumber' => $player->getRelationshipnumber(),
            'firstname' => $player->getFirstname(),
            'infix' => $player->getInfix(),
            'lastname' => $player->getLastname(),
            'gender' => $player->getGender(),
            'dateofbirth' => $player->getDateOfBirth(),
            'email' => $player->getEmail(),
            'team_id' => $player->getTeam_id(),
        ];
    
        return $this->json($data);
    }
}
