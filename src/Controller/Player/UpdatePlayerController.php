<?php

namespace App\Controller\Player;

use App\Service\PlayerService;
use App\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/player')]
class UpdatePlayerController extends AbstractController
{
    private PlayerService $playerService;
    private TeamRepository $teamRepository;

    public function __construct(PlayerService $playerService, TeamRepository $teamRepository)
    {
        $this->playerService = $playerService;
        $this->teamRepository = $teamRepository;
    }

    #[Route('/{id}', name: 'api_player_update', methods: ['PUT'])]
    public function update(Request $request, string $id): JsonResponse
    {
        $player = $this->playerService->getPlayerById($id);
        $currentUser = $this->getUser();

        $data = json_decode($request->getContent(), true);

        if (!$player) {
            return $this->json(['error' => 'player not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $player->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if (isset($data['membershipnumber'])) {
            $player->setMembershipnumber($data['membershipnumber']);
        }

        if (isset($data['relationshipnumber'])){
            $player->setRelationshipnumber($data['relationshipnumber']);
        }

        if (isset($data['firstname'])) {
            $player->setFirstname($data['firstname']);
        }
        
        if (isset($data['infix'])) {
            $player->setInfix($data['infix']);
        }

        if (isset($data['lastname'])) {
            $player->setLastname($data['lastname']);
        }

        if (isset($data['gender'])) {
            $player->setGender($data['gender']);
        }

        if (isset($data['email'])) {

            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                return $this->json(['error' => 'Invalid email format'], JsonResponse::HTTP_BAD_REQUEST);
            }
            
            $player->setEmail($data['email']);
        }

        if(isset($data['dateofbirth'])){    
            
            $date1 = \DateTime::createFromFormat('d-m-Y', $data['dateofbirth']);
            $date2 = \DateTime::createFromFormat('Y-m-d', $data['dateofbirth']);
            if (!$date1 && !$date2) {
                return $this->json(['error' => 'Invalid date'], JsonResponse::HTTP_BAD_REQUEST);
            }

            $player->setDateOfBirth($data['dateofbirth']);
        }

        if (isset($data['team_id'])) {
            $team = $this->teamRepository->getTeamByIdAndOrgId($data['team_id'], $player->getOrg_id());

            if (!$team) {
                return $this->json(['error' => 'Team not found'], JsonResponse::HTTP_NOT_FOUND);
            }

            $player->setTeam_id($team);
        }

        $this->playerService->updatePlayer($player);

        return $this->json($player->getId());
    }
}
