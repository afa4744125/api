<?php

namespace App\Controller\Player;

use App\Entity\Player;
use App\Service\PlayerService;
use App\Service\UserTeamAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;

#[Route('/api/players')]
class ListPlayerController extends AbstractController
{
    private PlayerService $playerService;
    private OrganisationRepository $organisationRepository;
    private UserTeamAccessService $userTeamAccessService;

    public function __construct(PlayerService $playerService, UserTeamAccessService $userTeamAccessService ,OrganisationRepository $organisationRepository)
    {
        $this->playerService = $playerService;
        $this->userTeamAccessService = $userTeamAccessService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/', name: 'api_player_list', methods: ['POST'])]
    public function getplayerlist(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $currentUser = $this->getUser();
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles());

        if (empty($data['org_id']) ) {
            return $this->json(['error' => 'Invalid input'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $organisation = $this->organisationRepository->findActiveById($data['org_id']);
        
        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $data['org_id'] && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if(isset($data['page'])){
            if($data['page'] < 1 ){
                return $this->json(['error' => 'Invalid page'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }

        if(isset($data['pagesize'])){
            if($data['pagesize'] < 1 ){
                return $this->json(['error' => 'Invalid pagesize'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }

        $offset = (($data['page'] ?? 1) - 1 ) * ($data['pagesize'] ?? 25);

        $filter = $data['filter'] ?? null;

        if (!$isAdmin) {
            $teamsAcces = $this->userTeamAccessService->findAllByUserIdGetIds($currentUser->getId());

            if (isset($filter['team_id']) && in_array((string) $filter['team_id'], $teamsAcces)) {
                $teams = [(string) $filter['team_id']];
            } else {
                $teams = $teamsAcces;
            }
            
        } else{
            $teams = isset($filter['team_id']) ? [$filter['team_id']] : [];
        }

        $player = $this->playerService->getPlayers( 
            $data['org_id'],
            ($data['pagesize']?? 25), 
            $offset,
            $filter['membershipnumber'] ?? null, 
            $filter['relationshipnumber'] ?? null, 
            $filter['firstname'] ?? null, 
            $filter['lastname'] ?? null, 
            $filter['email']  ?? null, 
            $teams, 
            $filter['gender'] ?? null, 
            $filter['dateofbirth'] ?? null, 
        );
        
        $output['players'] = array_map(function (Player $player) {
            return [
                'id' => $player->getId(),
                'membershipnumber' => $player->getMembershipnumber(),
                'firstname' => $player->getFirstname(),
                'infix' => $player->getInfix(),
                'lastname' => $player->getLastname(),
                'team_name' => $player->getTeam_name(),
                'gender' => $player->getGender(),
                'dateofbirth' => $player->getDateOfBirth(),
            ];
        }, $player);

        $output['totalPlayers'] =  $this->playerService->getPlayersCount(
            $data['org_id'],
            $filter['membershipnumber'] ?? null, 
            $filter['relationshipnumber'] ?? null, 
            $filter['firstname'] ?? null, 
            $filter['lastname'] ?? null, 
            $data['email']  ?? null, 
            $teams, 
            $filter['gender'] ?? null, 
            $filter['dateofbirth'] ?? null, 
        );

        return $this->json($output);
    }
}
