<?php

namespace App\Controller\Player;

use App\Entity\Player;
use App\Service\PlayerService;
use App\Repository\OrganisationRepository;
use App\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/player')]
class CreatePlayerController extends AbstractController
{
    private PlayerService $playerService;
    private OrganisationRepository $organisationRepository;
    private TeamRepository $teamRepository;

    public function __construct(PlayerService $playerService, OrganisationRepository $organisationRepository, TeamRepository $teamRepository)
    {
        $this->playerService = $playerService;
        $this->organisationRepository = $organisationRepository;
        $this->teamRepository = $teamRepository;
    }

    #[Route('/', name: 'api_player_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $currentUser = $this->getUser();

        if (empty($data['membershipnumber']) || empty($data['org_id']) || empty($data['firstname']) || empty($data['lastname']) || empty($data['dateofbirth']) || empty($data['gender']) ) {
            return $this->json(['error' => 'Invalid input'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $organisation = $this->organisationRepository->findActiveById($data['org_id']);
        
        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $data['org_id'] && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        $date1 = \DateTime::createFromFormat('d-m-Y', $data['dateofbirth']);
        $date2 = \DateTime::createFromFormat('Y-m-d', $data['dateofbirth']);
        if (!$date1 && !$date2) {
            return $this->json(['error' => 'Invalid date'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $player = new Player();

        $player->setMembershipnumber($data['membershipnumber']);

        if (isset($data['relationshipnumber'])){
            $player->setRelationshipnumber($data['relationshipnumber']);
        }

        $player->setFirstname($data['firstname']);
        
        if (isset($data['infix'])) {
            $player->setInfix($data['infix']);
        }

        $player->setLastname($data['lastname']);

            $player->setGender($data['gender']);

        if (isset($data['email'])) {

            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                return $this->json(['error' => 'Invalid email format'], JsonResponse::HTTP_BAD_REQUEST);
            }
            
            $player->setEmail($data['email']);
        }

        $player->setDateOfBirth($data['dateofbirth']);

        if (isset($data['team_id'])) {
            $team = $this->teamRepository->getTeamByIdAndOrgId($data['team_id'], $data['org_id']);

            if (!$team) {
                return $this->json(['error' => 'Team not found'], JsonResponse::HTTP_NOT_FOUND);
            }

            $player->setTeam_id($team);
        }
        
        $player->setOrg_id($organisation);

        $this->playerService->createPlayer($player);

        return $this->json(['id' => $player->getId()], JsonResponse::HTTP_CREATED);
    }
}
