<?php

namespace App\Controller\Organisation;

use App\Entity\Organisation;
use App\Service\OrganisationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/thema')]
class SetOrganisationThemaController extends AbstractController
{
    private OrganisationService $organisationService;
    private OrganisationRepository $organisationRepository;

    public function __construct(OrganisationService $organisationService, OrganisationRepository $organisationRepository)
    {
        $this->organisationService = $organisationService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/{id}', name: 'api_organisation_set_thema', methods: ['PUT'])]
    public function update(Request $request, string $id): JsonResponse
    {
        $organisation = $this->organisationService->getOrganisationById($id);
        $currentUser = $this->getUser();
    
        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }
    
        if ($currentUser->getOrg_id() !== $organisation->getid() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }
    
        $data = json_decode($request->getContent(), true);
        $settings = $organisation->getSettings();
    
        $hexPattern = '/^#[0-9A-Fa-f]{6}$/';

        if (isset($data['primaryColor'])) {
            if (preg_match($hexPattern, $data['primaryColor'])) {
                $settings['primaryColor'] = $data['primaryColor'];
            } else {
                return $this->json(['error' => 'Invalid hex format for primaryColor'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }
    
        if (isset($data['secondaryColor'])) {
            if (preg_match($hexPattern, $data['secondaryColor'])) {
                $settings['secondaryColor'] = $data['secondaryColor'];
            } else {
                return $this->json(['error' => 'Invalid hex format for secondaryColor'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }
    
        if (isset($data['tertiaryColor'])) {
            if (preg_match($hexPattern, $data['tertiaryColor'])) {
                $settings['tertiaryColor'] = $data['tertiaryColor'];
            } else {
                return $this->json(['error' => 'Invalid hex format for tertiaryColor'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }
    
        if (isset($data['logo'])) {
            $logoData = $data['logo'];
            $logoPath = $this->saveLogo($logoData, $id);
    
            if ($logoPath) {
                $settings['logo'] = $logoPath;
            } else {
                return $this->json([
                    'error' => 'Failed to save the image file'
                ], JsonResponse::HTTP_BAD_REQUEST);
            }
        }
    
        $organisation->setSettings($settings);
        $this->organisationService->updateOrganisation($organisation);
    
        return $this->json($organisation->getId());
    }

    private function saveLogo(string $logoData, string $organisationId): ?string
    {
        $organisation = $this->organisationService->getOrganisationById($organisationId);
    
        if (preg_match('/^data:image\/(\w+);base64,/', $logoData, $type)) {
            
            $base64 = substr($logoData, strpos($logoData, ',') + 1);
            $type = strtolower($type[1]);
            
            if (!in_array($type, ['jpg', 'jpeg', 'png'])) {
                return null;
            }
    
            $base64 = base64_decode($base64);
            if ($base64 === false) {
                return null; 
            }
    
            $sanitizedOrgName = str_replace(' ', '_', $organisation->getName());
            $directory = $this->getParameter('kernel.project_dir') . '/public/files/' . $sanitizedOrgName . '/';
            $fileName = 'logo.' . $type;
            $filePath = $directory . $fileName;
    
            if (!is_dir($directory)) {
                if (!mkdir($directory, 0755, true) && !is_dir($directory)) {
                    return $this->json(['error' => 'Could not create directory'], JsonResponse::HTTP_BAD_REQUEST);
                }
            }
    
            foreach (glob($directory . 'logo.*') as $existingFile) {
                if (file_exists($existingFile)) {
                    unlink($existingFile);
                }
            }
    
            if (file_put_contents($filePath, $base64) === false) {
                return $this->json(['error' => 'Failed to save image'], JsonResponse::HTTP_BAD_REQUEST);
            }
    
            return "files/{$sanitizedOrgName}/{$fileName}";
        }
    
        return null; 
    }
    
}
