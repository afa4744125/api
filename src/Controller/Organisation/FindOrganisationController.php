<?php

namespace App\Controller\Organisation;

use App\Entity\Organisation;
use App\Entity\User;
use App\Service\OrganisationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;

#[Route('/api/organisation')]
class FindOrganisationController extends AbstractController
{
    private OrganisationService $organisationService;
    private OrganisationRepository $organisationRepository;

    public function __construct(OrganisationService $organisationService, OrganisationRepository $organisationRepository)
    {
        $this->organisationService = $organisationService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/{id}', name: 'api_organisation_show', methods: ['GET'])]
    public function show(string $id): JsonResponse
    {
        $organisation = $this->organisationService->getOrganisationById($id);
        $currentUser = $this->getUser();
    
        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $id && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }
    
        $data = [
            'id' => $organisation->getId(),
            'name' => $organisation->getName(),
            'email' => $organisation->getEmail(),
            'settings' => $organisation->getSettings(),
        ];
    
        return $this->json($data);
    }
}
