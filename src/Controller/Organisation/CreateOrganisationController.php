<?php

namespace App\Controller\Organisation;

use App\Entity\Organisation;
use App\Service\OrganisationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/organisation')]
#[IsGranted('ROLE_SUPER_ADMIN')]
class CreateOrganisationController extends AbstractController
{
    private OrganisationService $organisationService;
    private OrganisationRepository $organisationRepository;

    public function __construct(OrganisationService $organisationService, OrganisationRepository $organisationRepository)
    {
        $this->organisationService = $organisationService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/', name: 'api_organisation_create', methods: ['POST'])]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data['name']) || empty($data['email'])) {
            return $this->json(['error' => 'Invalid input'], JsonResponse::HTTP_BAD_REQUEST);
        }

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return $this->json(['error' => 'Invalid email format'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $organisation = new Organisation();
        $organisation->setName($data['name']);
        $organisation->setEmail($data['email']);
        $organisation->setSettings($data['settings'] ?? []);

        $this->organisationService->createOrganisation($organisation);

        return $this->json($organisation->getId(), JsonResponse::HTTP_CREATED);
    }
}
