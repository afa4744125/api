<?php

namespace App\Controller\Organisation;

use App\Entity\Organisation;
use App\Service\OrganisationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/organisation')]
#[IsGranted('ROLE_SUPER_ADMIN')]
class DeleteOrganisationController extends AbstractController
{
    private OrganisationService $organisationService;
    private OrganisationRepository $organisationRepository;

    public function __construct(OrganisationService $organisationService, OrganisationRepository $organisationRepository)
    {
        $this->organisationService = $organisationService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/{id}', name: 'api_organisation_delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function delete(string $id): JsonResponse
    {
        $organisation = $this->organisationService->getOrganisationById($id);
    
        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }
    
        $this->organisationService->deleteOrganisation($organisation);
    
        return $this->json(['message' => 'Organisation deleted']);
    }    
}
