<?php

namespace App\Controller\Organisation;

use App\Entity\Organisation;
use App\Service\OrganisationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/organisations')]
#[IsGranted('ROLE_SUPER_ADMIN')]
class PublicListController extends AbstractController
{
    private OrganisationService $organisationService;
    private OrganisationRepository $organisationRepository;

    public function __construct(OrganisationService $organisationService, OrganisationRepository $organisationRepository)
    {
        $this->organisationService = $organisationService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/', name: 'api_organisation_publiclist', methods: ['GET'])]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function getAllOrganisations(): JsonResponse
    {
        $organisations = $this->organisationService->getAllOrganisations();

        if (!$organisations) {
            return $this->json(['error' => 'no Organisation found'], JsonResponse::HTTP_NOT_FOUND);
        }
        
        $data = array_map(function (Organisation $organisation) {
            return [
                'id' => $organisation->getId(),
                'name' => $organisation->getName(),
            ];
        }, $organisations);

        return $this->json($data);
    }
}
