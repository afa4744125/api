<?php

namespace App\Controller\Organisation;

use App\Entity\Organisation;
use App\Service\OrganisationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/settings/organisation')]
class GetOrganisationSettings extends AbstractController
{
    private OrganisationService $organisationService;
    private OrganisationRepository $organisationRepository;

    public function __construct(OrganisationService $organisationService, OrganisationRepository $organisationRepository)
    {
        $this->organisationService = $organisationService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/{id}', name: 'api_organisation_get_settings', methods: ['GET'])]
    public function getSettings(Request $request, string $id): JsonResponse
    {
        $organisation = $this->organisationService->getOrganisationById($id);
        $currentUser = $this->getUser();

        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $id && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to access this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        $data = [
            'email' =>  $organisation->getEmail(),
        ];

        return $this->json($data);
    }
}
