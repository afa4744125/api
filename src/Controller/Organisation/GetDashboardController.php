<?php

namespace App\Controller\Organisation;

use App\Entity\Organisation;
use App\Service\OrganisationService;
use App\Service\PlayerService;
use App\Service\TeamService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/dashboard')]
class GetDashboardController extends AbstractController
{
    private OrganisationService $organisationService;
    private OrganisationRepository $organisationRepository;
    private PlayerService $playerService;
    private TeamService $teamService;
    private UserService $userService;

    public function __construct(OrganisationService $organisationService, OrganisationRepository $organisationRepository, PlayerService $playerService, TeamService $teamService, UserService $userService)
    {
        $this->organisationService = $organisationService;
        $this->organisationRepository = $organisationRepository;
        $this->playerService = $playerService;
        $this->teamService = $teamService;
        $this->userService = $userService;
    }

    #[Route('/{org_id}', name: 'api_organisation_get_dashboard', methods: ['GET'])]
    public function getDashboard(string $org_id): JsonResponse
    {
        $organisation = $this->organisationService->getOrganisationById($org_id);
        $currentUser = $this->getUser();

        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $org_id && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to access this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        $data = [
            'totalPlayers' => $this->playerService->getPlayersCount($org_id),
            'totalPlayersDelete' => $this->playerService->getPlayersCountDelete($org_id),
            'totalTeams' =>  $this->teamService->getTeamsCount($org_id),
            'totalUsers' => $this->userService->getUsersCount($org_id),
        ];

        return $this->json($data);
    }
}
