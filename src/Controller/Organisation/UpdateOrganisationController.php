<?php

namespace App\Controller\Organisation;

use App\Entity\Organisation;
use App\Service\OrganisationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OrganisationRepository;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/organisation')]
#[IsGranted('ROLE_SUPER_ADMIN')]
class UpdateOrganisationController extends AbstractController
{
    private OrganisationService $organisationService;
    private OrganisationRepository $organisationRepository;

    public function __construct(OrganisationService $organisationService, OrganisationRepository $organisationRepository)
    {
        $this->organisationService = $organisationService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/{id}', name: 'api_organisation_update', methods: ['PUT'])]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function update(Request $request, string $id): JsonResponse
    {
        $organisation = $this->organisationService->getOrganisationById($id);

        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        $data = json_decode($request->getContent(), true);

        if (isset($data['name'])) {
            $organisation->setName($data['name']);
        }
        if (isset($data['email'])) {
            
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                return $this->json(['error' => 'Invalid email format'], JsonResponse::HTTP_BAD_REQUEST);
            }

            $organisation->setEmail($data['email']);
        }
        if (isset($data['settings'])) {
            $organisation->setSettings($data['settings']);
        }

        $this->organisationService->updateOrganisation($organisation);

        return $this->json($organisation->getId());
    }
}
