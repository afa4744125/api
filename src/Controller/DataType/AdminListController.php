<?php

namespace App\Controller\DataType;

use App\Entity\DataType;
use App\Service\DataTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/admin/datatypes')]
class AdminListController extends AbstractController
{
    private DataTypeService $dataTypeService;

    public function __construct(DataTypeService $dataTypeService)
    {
        $this->dataTypeService = $dataTypeService;
    }

    #[Route('/', name: 'api_datatype_adminlist', methods: ['GET'])]
    public function getAllDataTypes(): JsonResponse
    {
        $DataTypes = $this->dataTypeService->getAdminListDataType();

        if (!$DataTypes) {
            return $this->json(['error' => 'no datatype found'], JsonResponse::HTTP_NOT_FOUND);
        }
        
        $data = array_map(function (DataType $DataType) {
            return [
                'id' => $DataType->getId(),
                'name' => $DataType->getName(),
                'status' => $DataType->getStatus(),
                'number' => $DataType->getNumber(),
            ];
        }, $DataTypes);

        return $this->json($data);
    }
}