<?php

namespace App\Controller\DataType;

use App\Service\DataTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/datatype')]
class DeleteDataTypeController extends AbstractController
{
    private DataTypeService $dataTypeService;

    public function __construct(DataTypeService $dataTypeService)
    {
        $this->dataTypeService = $dataTypeService;
    }

    #[Route('/{id}', name: 'api_datatype_delete', methods: ['DELETE'])]
    public function delete(string $id): JsonResponse
    {
        $DataType = $this->dataTypeService->getDataTypeById($id);
    
        if (!$DataType) {
            return $this->json(['error' => 'DataType not found'], JsonResponse::HTTP_NOT_FOUND);
        }
    
        $this->dataTypeService->deleteDataType($DataType);
    
        return $this->json(['message' => 'DataType deleted']);
    }    
}
