<?php

namespace App\Controller\DataType;

use App\Service\DataTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/datatype')]
class FindDataTypeController extends AbstractController
{
    private DataTypeService $dataTypeService;

    public function __construct(DataTypeService $dataTypeService)
    {
        $this->dataTypeService = $dataTypeService;
    }

    #[Route('/{id}', name: 'api_datatype_show', methods: ['GET'])]
    public function show(string $id): JsonResponse
    {
        $DataType = $this->dataTypeService->getDataTypeById($id);
    
        if (!$DataType) {
            return $this->json(['error' => 'datatype not found'], JsonResponse::HTTP_NOT_FOUND);
        }
        $data = [
            'id' => $DataType->getId(),
            'name' => $DataType->getName(),
            'status' => $DataType->getStatus(),
            'number' => $DataType->getNumber(),
        ];
    
        return $this->json($data);
    }
}
