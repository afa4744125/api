<?php

namespace App\Controller\DataType;

use App\Service\DataTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/datatype')]
class UpdateDataTypeController extends AbstractController
{
    private DataTypeService $dataTypeService;

    public function __construct(DataTypeService $dataTypeService)
    {
        $this->dataTypeService = $dataTypeService;
    }

    #[Route('/{id}', name: 'api_datatype_update', methods: ['PUT'])]
    public function update(Request $request, string $id): JsonResponse
    {
        $DataType = $this->dataTypeService->getDataTypeById($id);

        if (!$DataType) {
            return $this->json(['error' => 'datatype not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        $data = json_decode($request->getContent(), true);

        if (isset($data['name'])) {
            $DataType->setName($data['name']);
        }
        if (isset($data['status'])) {
            $DataType->setStatus($data['status']);
        }
        if (isset($data['number'])) {
            $DataType->setNumber(filter_var($data['number'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE));
        }

        $this->dataTypeService->updateDataType();

        return $this->json($DataType->getId());
    }
}
