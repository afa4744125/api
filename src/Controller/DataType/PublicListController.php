<?php

namespace App\Controller\DataType;

use App\Entity\DataType;
use App\Service\DataTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/datatypes')]
class PublicListController extends AbstractController
{
    private DataTypeService $dataTypeService;

    public function __construct(DataTypeService $dataTypeService)
    {
        $this->dataTypeService = $dataTypeService;
    }

    #[Route('/', name: 'api_datatype_publiclist', methods: ['GET'])]
    public function getAllDataTypes(): JsonResponse
    {
        $DataTypes = $this->dataTypeService->getPublicListDataType();

        if (!$DataTypes) {
            return $this->json(['error' => 'no datatype found'], JsonResponse::HTTP_NOT_FOUND);
        }
        
        $data = array_map(function (DataType $DataType) {
            return [
                'id' => $DataType->getId(),
                'name' => $DataType->getName(),
                'number' => $DataType->getNumber(),
            ];
        }, $DataTypes);

        return $this->json($data);
    }
}