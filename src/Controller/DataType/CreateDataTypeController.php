<?php

namespace App\Controller\DataType;

use App\Entity\DataType;
use App\Service\DataTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/datatype')]
class CreateDataTypeController extends AbstractController
{
    private DataTypeService $dataTypeService;

    public function __construct(DataTypeService $dataTypeService)
    {
        $this->dataTypeService = $dataTypeService;
    }

    #[Route('/', name: 'api_datatype_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data['name'])) {
            return $this->json(['error' => 'Invalid input'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $DataType = new DataType();
        $DataType->setName($data['name']);
        $DataType->setStatus($data['status'] ?? 'inactive');
        $DataType->setNumber(isset($data['number']) ? filter_var($data['number'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : true);
        

        $this->dataTypeService->createDataType($DataType);

        return $this->json($DataType->getId(), JsonResponse::HTTP_CREATED);
    }
}
