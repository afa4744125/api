<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;

#[Route('/api/{org_id}/users')]
class GetAllUsersController extends AbstractController
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    #[Route('/', name: 'api_user_show', methods: ['GET'])]
    public function show(string $org_id): JsonResponse
    {
        $user = $this->userService->getAllUsers($org_id);
        $currentUser = $this->getUser();
    
        if (!$user) {
            return $this->json(['error' => 'user not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $org_id && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }
    
        $data = array_map(function (User $user) {
            return [
                'id' => $user->getId(),
                'firstname' => $user->getFirstname(),
                'lastname' => $user->getLastname(),
                'role' => $user->getRoles(),
            ];
        }, $user);
    
        return $this->json($data);
    }
}
