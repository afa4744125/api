<?php

namespace App\Controller\User;

use App\Service\UserService;
use App\Service\UserTeamAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/user')]
class DeleteUserController extends AbstractController
{
    private UserService $userService;
    private UserTeamAccessService $userTeamAccessService; 

    public function __construct(UserService $userService, UserTeamAccessService $userTeamAccessService)
    {
        $this->userService = $userService;
        $this->userTeamAccessService = $userTeamAccessService;
    }

    #[Route('/{id}', name: 'api_user_delete', methods: ['DELETE'])]
    public function delete(string $id): JsonResponse
    {
        $user = $this->userService->getUserById($id);
        $currentUser = $this->getUser();
    
        if (!$user) {
            return $this->json(['error' => 'user not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $user->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        $this->userTeamAccessService->deleteAllByUserId($user->getId());

        $this->userService->deleteUser($user);
    
        return $this->json(['message' => 'user deleted']);
    }    
}
