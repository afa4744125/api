<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Entity\UserTeamAccess;
use App\Service\UserService;
use App\Service\UserTeamAccessService; 
use App\Repository\TeamRepository;
use App\Repository\OrganisationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


#[Route('/api/user')]
class CreateUserController extends AbstractController
{
    private UserService $userService;
    private UserTeamAccessService $userTeamAccessService;
    private OrganisationRepository $organisationRepository;
    private UserPasswordHasherInterface $passwordHasher;
    private TeamRepository $teamRepository;

    public function __construct(
        UserService $userService,
        OrganisationRepository $organisationRepository,
        UserPasswordHasherInterface $passwordHasher,
        TeamRepository $teamRepository,
        UserTeamAccessService $userTeamAccessService
    )
    {
        $this->userService = $userService;
        $this->userTeamAccessService = $userTeamAccessService;
        $this->organisationRepository = $organisationRepository;
        $this->passwordHasher = $passwordHasher;
        $this->teamRepository = $teamRepository;
    }
    

    #[Route('/', name: 'api_user_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $organisation = $this->organisationRepository->findActiveById($data['org_id']);
        $currentUser = $this->getUser();

        if (empty($data['firstname']) || empty($data['lastname']) || empty($data['email']) || empty($data['org_id']) || empty($data['password'])) {
            return $this->json(['error' => 'Invalid input'], JsonResponse::HTTP_BAD_REQUEST);
        }

        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $data['org_id'] && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        //check if role is validate
        if (isset($data['role'])) {
            if (is_array($data['role'])) {
                foreach ($data['role'] as $role) {
                    if (!in_array(strtoupper($role), ['ROLE_USER', 'ROLE_ADMIN'])) {
                        return $this->json(['error' => 'Invalid role found'], JsonResponse::HTTP_BAD_REQUEST);
                    }
                }
            }
            else if (is_string($data['role']) && in_array(strtoupper($data['role']), ['ROLE_USER', 'ROLE_ADMIN'])) {
                $data['role'] = [strtoupper($data['role'])];
            }
            else {
                return $this->json(['error' => 'Invalid role found'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }

        if (empty($data['role']) || !in_array('ROLE_ADMIN', $data['role'])) {
            if(!isset($data['teams']) || !is_array($data['teams'])){
                return $this->json(['error' => 'User you want to create has role_user but no teams'], JsonResponse::HTTP_BAD_REQUEST);
            }

            foreach ($data['teams'] as $teamId) {
                $team = $this->teamRepository->getTeamByIdAndOrgId($teamId, $data['org_id']);
                if (!$team) {
                    return $this->json(['error' => "Invalid teams"], JsonResponse::HTTP_BAD_REQUEST);
                }
            }
        }
        
        $User = new User();
        $hashedPassword = $this->passwordHasher->hashPassword($User, $data['password']);
        $User->setPassword($hashedPassword);

        $User->setFirstname($data['firstname']);
        $User->setLastname($data['lastname']);
        $User->setEmail($data['email']);
        $User->setOrg_id($organisation);
        $User->setRoles(isset($data['role']) && is_array($data['role']) ? $data['role'] : []);

        $this->userService->createUser($User);
        
        if (empty($data['role']) || !in_array('ROLE_ADMIN', $data['role'])) {
            foreach ($data['teams'] as $teamId) {
                $Team = $this->teamRepository->getTeamByIdAndOrgId($teamId, $data['org_id']);
                if ($Team) {
                    $UserTeamAccess = new UserTeamAccess();

                    $UserTeamAccess->setUser_id($User);
                    $UserTeamAccess->setTeam_id($Team);

                    $this->userTeamAccessService->createUserTeamAccess($UserTeamAccess);
                }
            }
        }

        return $this->json($User->getId(), JsonResponse::HTTP_CREATED);
    }
}
