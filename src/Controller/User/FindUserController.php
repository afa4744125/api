<?php

namespace App\Controller\User;

use App\Service\UserService;
use App\Service\UserTeamAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/user')]
class FindUserController extends AbstractController
{
    private UserService $userService;
    private UserTeamAccessService $userTeamAccessService;

    public function __construct(UserService $userService, UserTeamAccessService $userTeamAccessService)
    {
        $this->userService = $userService;
        $this->userTeamAccessService = $userTeamAccessService;
    }

    #[Route('/{id}', name: 'api_user_find', methods: ['GET'])]
    public function show(string $id): JsonResponse
    {
        $user = $this->userService->getUserById($id);
        $currentUser = $this->getUser();
    
        if (!$user) {
            return $this->json(['error' => 'User not found'], JsonResponse::HTTP_NOT_FOUND);
        }
    
        if ($currentUser->getId() !== $id && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles()) && !in_array('ROLE_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this user information"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if ($currentUser->getOrg_id() !== $user->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisationn"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        $data = [
            'id' => $user->getId(),
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getLastname(),
            'email' => $user->getEmail(),
            'role' => $user->getRoles(),
            'teams' => $this->userTeamAccessService->findAllByUserIdGetIds($user->getId()),
        ];
    
        return $this->json($data);
    }
}
