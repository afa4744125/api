<?php

namespace App\Controller\User;

use App\Entity\UserTeamAccess;
use App\Service\UserService;
use App\Service\UserTeamAccessService; 
use App\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/user')]
class UpdateUserController extends AbstractController
{
    private UserService $userService;
    private UserTeamAccessService $userTeamAccessService;
    private TeamRepository $teamRepository;

    public function __construct(
        UserService $userService,
        UserTeamAccessService $userTeamAccessService,
        TeamRepository $teamRepository,
    )
    {
        $this->userService = $userService;
        $this->userTeamAccessService = $userTeamAccessService;
        $this->teamRepository = $teamRepository;
    }

    #[Route('/{id}', name: 'api_user_update', methods: ['PUT'])]
    public function update(Request $request, string $id): JsonResponse
    {
        $user = $this->userService->getUserById($id);
        $currentUser = $this->getUser();

        $data = json_decode($request->getContent(), true);

        if (!$user) {
            return $this->json(['error' => 'User not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getId() !== $id && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles()) && !in_array('ROLE_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this user information"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if ($currentUser->getOrg_id() !== $user->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if (isset($data['role'])) {
            if (is_array($data['role'])) {
                foreach ($data['role'] as $role) {
                    if (!in_array(strtoupper($role), ['ROLE_USER', 'ROLE_ADMIN'])) {
                        return $this->json(['error' => 'Invalid role found'], JsonResponse::HTTP_BAD_REQUEST);
                    }
                }
            }
            else if (is_string($data['role']) && in_array(strtoupper($data['role']), ['ROLE_USER', 'ROLE_ADMIN'])) {
                $data['role'] = [strtoupper($data['role'])];
            }
            else {
                return $this->json(['error' => 'Invalid role found'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }

        if (
            (!in_array('ROLE_ADMIN', $user->getRoles()) && !in_array('ROLE_SUPER_ADMIN', $user->getRoles())) && isset($data['teams'])
            || 
            (isset($data['role']) && !in_array('ROLE_ADMIN', $data['role']))
        ) {

            if (
                (empty($data['teams']) || !is_array($data['teams']))
                &&
                (isset($data['role']) && (!in_array('ROLE_ADMIN', $data['role'])))
            ) {
                return $this->json(['error' => 'User you want to create has role_user but no teams empty or no arra'], JsonResponse::HTTP_BAD_REQUEST);
            }
        
            if (!empty($data['teams']) && is_array($data['teams'])) {
                foreach ($data['teams'] as $teamId) {
                    $team = $this->teamRepository->getTeamByIdAndOrgId($teamId, $user->getOrg_id());
                    if (!$team) {
                        return $this->json(['error' => "Invalid teams"], JsonResponse::HTTP_BAD_REQUEST);
                    }
                }
            } else {
                return $this->json(['error' => 'teams is empty or is no array'], JsonResponse::HTTP_BAD_REQUEST);
            }
        }
        
        
        if (isset($data['firstname'])) {
            $user->setFirstname($data['firstname']);
        }

        if (isset($data['lastname'])) {
            $user->setLastname($data['lastname']);
        }

        if (isset($data['email'])) {
            $user->setEmail($data['email']);
        }

        if (isset($data['role'])) {
            $user->setRoles(isset($data['role']) && is_array($data['role']) ? $data['role'] : []);
        }

        $this->userService->updateUser($user);

        if (
            (!in_array('ROLE_ADMIN', $user->getRoles()) && !in_array('ROLE_SUPER_ADMIN', $user->getRoles()) && isset($data['teams']))
            || 
            (isset($data['role']) && !in_array('ROLE_ADMIN', $data['role']))
        ) { 
            
            $this->userTeamAccessService->deleteAllByUserId($user->getId());
        
            foreach ($data['teams'] as $teamId) {
                $Team = $this->teamRepository->getTeamByIdAndOrgId($teamId, $user->getOrg_id());
                if ($Team) {
                    $UserTeamAccess = new UserTeamAccess();
        
                    $UserTeamAccess->setUser_id($user);
                    $UserTeamAccess->setTeam_id($Team);
    
                    $this->userTeamAccessService->createUserTeamAccess($UserTeamAccess);
                }
            }

        }

        return $this->json($user->getId());
    }
}
