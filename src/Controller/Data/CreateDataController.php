<?php

namespace App\Controller\Data;

use App\Entity\Data;
use App\Service\DataService;
use App\Service\PlayerService;
use App\Service\UserTeamAccessService;
use App\Service\DataTypeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/data')]
class CreateDataController extends AbstractController
{
    private DataService $dataService;
    private PlayerService $playerService;
    private DataTypeService $dataTypeService;
    private UserTeamAccessService $userTeamAccessService;

    public function __construct(DataService $dataService, PlayerService $playerService, DataTypeService $dataTypeService, UserTeamAccessService $userTeamAccessService)
    {
        $this->dataService = $dataService;
        $this->playerService = $playerService;
        $this->dataTypeService = $dataTypeService;
        $this->userTeamAccessService = $userTeamAccessService;
    }

    #[Route('/', name: 'api_data_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $currentUser = $this->getUser();
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles());

        if (empty($data['value']) || empty($data['player_id']) || empty($data['datatype_id'])) {
            return $this->json(['error' => 'Invalid input'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $player = $this->playerService->getPlayerById($data['player_id']);
        $dataType = $this->dataTypeService->getDataTypeById($data['datatype_id']);

        if (!$player) {
            return $this->json(['error' => 'Player not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if (!$dataType) {
            return $this->json(['error' => 'Data type not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if($dataType->getStatus() !== 'active'){
            return $this->json(['error' => 'Data type is inactive'], JsonResponse::HTTP_BAD_REQUEST);
        }

        if ($currentUser->getOrg_id() !== $player->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if (!$isAdmin) {
            $hasAccess = false;
        
            if ($player->getTeam_id()) {
                $hasAccess = $this->userTeamAccessService->hasUserAccessToTeam($currentUser->getId(), $player->getTeam_id());
            }
        
            if (!$hasAccess) {
                return $this->json([
                    'error' => "You do not have permission to use this team"
                ], JsonResponse::HTTP_FORBIDDEN);
            }
        }

        $Data = new Data();
        $Data->setValue($data['value']);
        $Data->setPlayer_Id($player);
        $Data->setDataType_Id($dataType);
        $Data->setCreatedBy($currentUser);

        $this->dataService->createData($Data);

        return $this->json($Data->getId(), JsonResponse::HTTP_CREATED);
    }
}
