<?php

namespace App\Controller\Data;

use App\Service\DataService;
use App\Service\PlayerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/data')]
class FindDataController extends AbstractController
{
    private DataService $dataService;
    private PlayerService $playerService;

    public function __construct(DataService $dataService, PlayerService $playerService)
    {
        $this->dataService = $dataService;
        $this->playerService = $playerService;
    }

    #[Route('/{id}', name: 'api_data_find', methods: ['GET'])]
    public function find(string $id): JsonResponse
    {
        $Data = $this->dataService->getDataById($id);
        $currentUser = $this->getUser();
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles());

        if (!$Data) {
            return $this->json(['error' => 'Data not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        $player = $this->playerService->getPlayerById($Data->getPlayer_id());

        // Check if the current user has permission to delete the data in this organsation
        if ($currentUser->getOrg_id() !== $player->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        // Check if the current user has permission to delete the data in this and made the data
        if(!$isAdmin) {
            if ($currentUser->getId() !== $Data->getCreatedBy()->getId() ) {
                return $this->json([
                    'error' => "You do not have permission to use this player data"
                ], JsonResponse::HTTP_FORBIDDEN);
            }

        }
    
        $data = [
            'id' => $Data->getId(),
            'value' => $Data->getValue(),
            'player' => $Data->getPlayer_id(),
            'createdby' => $Data->getCreatedBy()->getFirstname() . ' ' .  $Data->getCreatedBy()->getLastname(),
            'datatype_name' => $Data->getDataType_id()->getName(),
            'datatype_id' => $Data->getDataType_id()->getId(),
        ];
    
        return $this->json($data);
    }    
}
