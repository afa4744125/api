<?php

namespace App\Controller\Data;

use App\Entity\Data;
use App\Service\DataService;
use App\Service\PlayerService;
use App\Service\UserTeamAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/player/data')]
class FindDataOfPlayerController extends AbstractController
{
    private DataService $dataService;
    private PlayerService $playerService;
    private UserTeamAccessService $userTeamAccessService;

    public function __construct(DataService $dataService, PlayerService $playerService, UserTeamAccessService $userTeamAccessService)
    {
        $this->dataService = $dataService;
        $this->playerService = $playerService;
        $this->userTeamAccessService = $userTeamAccessService;
    }

    #[Route('/{id}', name: 'api_data_find_of_player', methods: ['GET'])]
    public function show(string $id): JsonResponse
    {
        $Datas = $this->dataService->getDataOfPlayer($id);
        $currentUser = $this->getUser();
        $player = $this->playerService->getPlayerById($id);
    
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles());

        if (!$Datas) {
            return $this->json(['error' => 'Data not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if (!$player) {
            return $this->json(['error' => 'Player not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $player->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if (!$isAdmin) {
            $hasAccess = false;
        
            if ($player->getTeam_id()) {
                $hasAccess = $this->userTeamAccessService->hasUserAccessToTeam($currentUser->getId(), $player->getTeam_id());
            }
        
            if (!$hasAccess) {
                return $this->json([
                    'error' => "You do not have permission to use this team"
                ], JsonResponse::HTTP_FORBIDDEN);
            }
        }

        $data = array_map(function (Data $Data) {
            return [
                'id' => $Data->getId(),
                'value' => $Data->getValue(),
                'createdty' => $Data->getCreatedBy()->getFirstname() . ' ' .  $Data->getCreatedBy()->getLastname(),
                'datatype_name' => $Data->getDataType_id()->getName(),
                'datatype_id' => $Data->getDataType_id()->getId(),
            ];
        }, $Datas);
    
        return $this->json($data);
    }
}
