<?php

namespace App\Controller\Data;

use App\Service\DataService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/data')]
class UpdateDataController extends AbstractController
{
    private DataService $dataService;

    public function __construct(DataService $dataService)
    {
        $this->dataService = $dataService;
    }

    #[Route('/{id}', name: 'api_data_update', methods: ['PUT'])]
    public function update(Request $request, string $id): JsonResponse
    {
        $Data = $this->dataService->getDataById($id);
        $currentUser = $this->getUser();
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles());

        if (!$Data) {
            return $this->json(['error' => 'data not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if(!$isAdmin) {
            
            if ($currentUser->getId() !== $Data->getCreatedBy()->getId() ) {
                return $this->json([
                    'error' => "You do not have permission to use this player data"
                ], JsonResponse::HTTP_FORBIDDEN);
            }

        }

        $data = json_decode($request->getContent(), true);

        if (isset($data['value'])) {
            $Data->setValue($data['value']);
        }

        $this->dataService->updateData();

        return $this->json($Data->getId());
    }
}
