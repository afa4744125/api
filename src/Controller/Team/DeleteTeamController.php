<?php

namespace App\Controller\Team;

use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/team')]
class DeleteTeamController extends AbstractController
{
    private TeamService $teamService;

    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
    }

    #[Route('/{id}', name: 'api_Team_delete', methods: ['DELETE'])]
    public function delete(string $id): JsonResponse
    {
        $team = $this->teamService->getTeamById($id);
        $currentUser = $this->getUser();
    
        if (!$team) {
            return $this->json(['error' => 'Team not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $team->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }
    
        $this->teamService->deleteTeam($team);
    
        return $this->json(['message' => 'Team deleted']);
    }    
}
