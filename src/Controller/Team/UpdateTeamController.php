<?php

namespace App\Controller\Team;

use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/team')]
class UpdateTeamController extends AbstractController
{
    private TeamService $teamService;

    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
    }

    #[Route('/{id}', name: 'api_team_update', methods: ['PUT'])]
    public function update(Request $request, string $id): JsonResponse
    {
        $team = $this->teamService->getTeamById($id);
        $currentUser = $this->getUser();

        $data = json_decode($request->getContent(), true);

        if (!$team) {
            return $this->json(['error' => 'Team not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $team->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if (isset($data['name'])) {
            $team->setName($data['name']);
        }

        $this->teamService->updateTeam($team);

        return $this->json($team->getId());
    }
}
