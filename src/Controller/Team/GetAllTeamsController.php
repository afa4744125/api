<?php

namespace App\Controller\Team;

use App\Entity\Team;
use App\Service\TeamService;
use App\Service\UserTeamAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/{org_id}/teams')]
class GetAllTeamsController extends AbstractController
{
    private TeamService $teamService;
    private UserTeamAccessService $userTeamAccessService;

    public function __construct(TeamService $teamService, UserTeamAccessService $userTeamAccessService)
    {
        $this->teamService = $teamService;
        $this->userTeamAccessService = $userTeamAccessService;
    }

    #[Route('/', name: 'api_team_show', methods: ['GET'])]
    public function show(string $org_id): JsonResponse
    {
        $currentUser = $this->getUser();
        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles());

        if ($currentUser->getOrg_id() !== $org_id && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if ($isAdmin) {
            $teams = $this->teamService->getTeamsById($org_id);
            
        } else {
            $teams = $this->userTeamAccessService->findAllByUserIdGetTeams($currentUser->getId());
        }

        if (!$teams) {
            return $this->json(['error' => 'Team not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        $data = array_map(function (Team $team) {
            return [
                'id' => $team->getId(),
                'name' => $team->getName(),
            ];
        }, $teams);

        return $this->json($data);
    }
}
