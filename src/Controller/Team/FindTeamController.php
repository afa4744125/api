<?php

namespace App\Controller\Team;

use App\Service\TeamService;
use App\Service\UserTeamAccessService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/team')]
class FindTeamController extends AbstractController
{
    private TeamService $teamService;
    private UserTeamAccessService $userTeamAccessService;

    public function __construct(TeamService $teamService, UserTeamAccessService $userTeamAccessService)
    {
        $this->teamService = $teamService;
        $this->userTeamAccessService = $userTeamAccessService;
    }

    #[Route('/{id}', name: 'api_team_find', methods: ['GET'])]
    public function show(string $id): JsonResponse
    {
        $team = $this->teamService->getTeamById($id);
        $currentUser = $this->getUser();

        $isAdmin = in_array('ROLE_ADMIN', $currentUser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles());
    
        if (!$team) {
            return $this->json(['error' => 'team not found'], JsonResponse::HTTP_NOT_FOUND);
        }
    
        if ($currentUser->getOrg_id() !== $team->getOrg_id() && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisationn"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        if (!$isAdmin) {
            $hasAccess = $this->userTeamAccessService->hasUserAccessToTeam($currentUser->getId(), $team->getId());

            if (!$hasAccess) {
                return $this->json([
                    'error' => "You do not have permission to use this team"
                ], JsonResponse::HTTP_FORBIDDEN);
            }
        }

        $data = [
            'id' => $team->getId(),
            'name' => $team->getName(),
        ];
    
        return $this->json($data);
    }
}
