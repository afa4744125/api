<?php

namespace App\Controller\Team;

use App\Entity\Team;
use App\Service\TeamService;
use App\Repository\OrganisationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/team')]
class CreateTeamController extends AbstractController
{
    private TeamService $teamService;
    private OrganisationRepository $organisationRepository;

    public function __construct(TeamService $teamService, OrganisationRepository $organisationRepository)
    {
        $this->teamService = $teamService;
        $this->organisationRepository = $organisationRepository;
    }

    #[Route('/', name: 'api_team_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $currentUser = $this->getUser();

        if (empty($data['name']) || empty($data['org_id'])) {
            return $this->json(['error' => 'Invalid input'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $organisation = $this->organisationRepository->findActiveById($data['org_id']);
        
        if (!$organisation) {
            return $this->json(['error' => 'Organisation not found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if ($currentUser->getOrg_id() !== $data['org_id'] && !in_array('ROLE_SUPER_ADMIN', $currentUser->getRoles())) {
            return $this->json([
                'error' => "You do not have permission to use this organisation"
            ], JsonResponse::HTTP_FORBIDDEN);
        }

        $team = new Team();
        $team->setName($data['name']);
        $team->setOrg_id($organisation);

        $this->teamService->createTeam($team);

        return $this->json(['id' => $team->getId()], JsonResponse::HTTP_CREATED);
    }
}
